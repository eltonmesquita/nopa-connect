import React from 'react';


const Transactions = (props) => {
  const transactions = props.transactions.map((transaction) => {
    return (
      <div className="transaction" key={ transaction.id }>
        <div className="transaction-beneficary">{ transaction.beneficary }</div>
        <div className="transaction-value">{ transaction.value }</div>
      </div>
    )
  })

  return (
    <section className="transaction-wrap">
      <h3 className="transaction-day">{ props.day} </h3>
      { transactions }
    </section>
  )
}

Transactions.propTypes = {
  transactions: React.PropTypes.array.isRequired,
  day: React.PropTypes.string.isRequired
};

export default Transactions;
