import React from 'react';

const partners = [{
    name: 'Airbnb',
    logo: require('../../../static/images/Airbnb.png')
  },
  {
    name: 'Metro',
    logo: require('../../../static/images/Metro.png')
  },
  {
    name: 'Pariti',
    logo: require('../../../static/images/Pariti.png')
  },
  {
    name: 'Unshackled',
    logo: require('../../../static/images/Unshackled.png')
  }
];

const partnerLogos = partners.map((partner) =>
    <li key={partner.name}><img className="Nopa" alt={partner.name} src={partner.logo}/></li>
);

const Partners = (props) => {
  return (
    <section className="partners">
      <div className="wrap">
        <h4>
          Our partners:
        </h4>
        <ul className="partners-list">
          {partnerLogos}
        </ul>
      </div>
    </section>
  );
};

export default Partners;
