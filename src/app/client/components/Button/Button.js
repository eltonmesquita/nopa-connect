import React from 'react';
import { Link } from 'react-router';

const Button = (props) => {
  return (
    <div className={props.className}>
      <Link {...props} className="button">
        <span>{props.children}</span>
      </Link>
    </div>
  )
};

export default Button;
