import React from 'react';
import Button from '../Button/Button';
import * as Paths from '../../constants/paths';
import { Link } from 'react-router';

const Header = (props) => {
  return (
    <div className="header">
      <div className="logo">
        <Link to={Paths.HOME} className="logo">
          <img className="Nopa" alt="Logo" src={require('../../../static/images/Logo_Nopa.svg')} />
        </Link>
      </div>
      <div className="signin">
        <Button to={Paths.LOGIN_BANK}>Log In</Button>
      </div>
    </div>
  )
};

export default Header;
