import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const TextInput = (props) => {
  return (
    <div className="form-input">
      <label
        htmlFor={props.name}>
        {props.label}
      </label>
      <input
        id={props.name}
        name={props.name}
        type={props.type}
        placeholder={props.label}
        onChange={props.handleChange}
        data-error={props.errors} />
      { props.errors &&
        <CSSTransitionGroup
          transitionName="bank-error"
          transitionAppear={true}
          transitionAppearTimeout={600}
          transitionEnterTimeout={600}
          transitionLeave={false}>
          <div className="errors">
            <span>{props.errors}</span>
          </div>
        </CSSTransitionGroup> }
    </div>
  );
};

TextInput.propTypes = {
  name: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  handleChange: React.PropTypes.func.isRequired,
  password: React.PropTypes.bool,
  errors: React.PropTypes.string
};

export default TextInput;
