import React from 'react';
import { Layout, Button } from '../../components';
import { Link } from 'react-router';
import * as Paths from '../../constants/paths';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

class HomePage extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      windowWidth: null
    }
    this.listenWindowWidth = this.listenWindowWidth.bind(this)
  }

  listenWindowWidth() {
    let windowWidth = document.documentElement.getBoundingClientRect().width
    this.setState({
      windowWidth
    })
  }

  componentWillMount() {
    window.addEventListener('resize', this.listenWindowWidth)
    this.listenWindowWidth()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.listenWindowWidth)
  }

  render () {
    const logo = this.state.windowWidth > 600 &&
      <img className="Nopa" alt="Logo" src={require('../../../static/images/Logo_Nopa.svg')} key="logo" />

    return (
      <Layout title="Welcome to Nopa!" cssClasses="home-page">
        <div className="main-content">
          <div className="logo">
            <CSSTransitionGroup
              transitionName="hero-logo"
              transitionAppear={true}
              transitionAppearTimeout={800}
              transitionEnterTimeout={800}
              transitionLeave={false}>
              { logo }
            </CSSTransitionGroup>
          </div>

          <div className="hero-title">
            <CSSTransitionGroup
              transitionName="hero-title"
              transitionAppear={true}
              transitionAppearTimeout={800}
              transitionEnterTimeout={800}
              transitionLeave={false}>
              <h1>Your finances, in one place</h1>
            </CSSTransitionGroup>
          </div>

          <div className="hero-copy">
            <CSSTransitionGroup
              transitionName="hero-copy"
              transitionAppear={true}
              transitionAppearTimeout={800}
              transitionEnterTimeout={800}
              transitionLeave={false}>
              <p>Track all of your payments by connecting as many bank accounts as<br className="hide-for-small" />
              you'd like to your Nopa account and get updates on your balance instantly.</p>
            </CSSTransitionGroup>
          </div>

          <div className="hero-button">
            <CSSTransitionGroup
              transitionName="hero-button"
              transitionAppear={true}
              transitionAppearTimeout={600}
              transitionEnterTimeout={600}
              transitionLeave={false}>
              <Button to={Paths.CHOOSE_BANK}>Get started</Button>
            </CSSTransitionGroup>
          </div>
        </div>

        <div className="secondary-content">
          <div className="wrap">
            <div className="secondary-content-copy">
              <h2>There's no such things as "one size fits all" finance.</h2>
              <p>We were founded to make money simple and fair for everyone: whether you're looking for a loan, or to get better rewards for your investments.</p>
            </div>
            <div className="shapes-wrap">
              <img className="shapes" alt="Shapes" src={require('../../../static/images/Shapes.svg')} />
            </div>
          </div>
        </div>

      </Layout>
    );

  }
}

export default HomePage;
