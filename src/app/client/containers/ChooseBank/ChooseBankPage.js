import React from 'react';
import { Layout, Button } from '../../components';
import * as Paths from '../../constants/paths';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const bankList = [
  {
    name: 'Barclays',
    logo: require('../../../static/images/Barclays.png')
  },
  {
    name: 'Natwest',
    logo: require('../../../static/images/LogoNatwest.png')
  },
  {
    name: 'Lloyds',
    logo: require('../../../static/images/LogoLloyds.png')
  },
  {
    name: 'HSBC',
    logo: require('../../../static/images/LogoHSBC.png')
  },
  {
    name: 'TSB',
    logo: require('../../../static/images/LogoTSB.png')
  },
  {
    name: 'Santander',
    logo: require('../../../static/images/LogoSantander.png')
  }
];

class ChooseBankPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      banks: bankList.map(item => {
        item.isSelected = false
        return item
      }),
      error: false
    }
  }

  handleBankClick (bank) {
    this.setState({ error: false })

    let clonedBanks = this.state.banks.map(item => {
      if(item.name === bank.name) {
        item.isSelected = !item.isSelected
        return item
      } else {
        item.isSelected = false
        return item
      }

    })

    this.setState({ banks: clonedBanks })
  }

  render() {
    const banks = this.state.banks.map(bank => {
      return (
        <li
          key={bank.name}
          className={ bank.isSelected ? 'selected' : '' }
          onClick={() => this.handleBankClick(bank)}>
            <div className="bank-item">
              <img alt={bank.name} src={bank.logo} />
            </div>
        </li>
      )
    })

    const returnButton = () => {
      let isValid = false

      this.state.banks.forEach(item => {
        if(item.isSelected) isValid = true
      }, this)

      return isValid ?
                <Button to={Paths.LOGIN_BANK}>Connect</Button> :
                <Button className="disabled" onClick={ () => this.setState({ error: true }) }>Connect</Button>
    }

    return (
      <Layout title="Nopa - Choose yor Bank">
        <div className="main-content choose-bank">
          <CSSTransitionGroup
            transitionName="hero-choosebank"
            transitionAppear={true}
            transitionAppearTimeout={400}
            transitionEnterTimeout={400}
            transitionLeave={false}>
            <div className="hero-choosebank">
              <h1>Which bank does this account belong to?</h1>
              <p className="hide-for-small">Track all of your payments by connecting as many bank accounts as you'd like to your Nopa<br />
                account and get updates on your balance instantly.</p>
              <p className="show-for-small">Choose your bank</p>
            </div>
          </CSSTransitionGroup>

          { this.state.error &&
            <CSSTransitionGroup
              transitionName="bank-error"
              transitionAppear={true}
              transitionAppearTimeout={600}
              transitionEnterTimeout={600}
              transitionLeave={false}>
                <div className="errors">
                  <span>Please choose a bank</span>
                </div>
              </CSSTransitionGroup> }

          <div className="bank-selection">
            <CSSTransitionGroup
              transitionName="bank-items"
              transitionAppear={true}
              transitionAppearTimeout={1000}
              transitionEnterTimeout={1000}
              transitionLeave={false}>
              <ul className="bank-list">
                  { banks }
              </ul>
            </CSSTransitionGroup>

            <div className="button-wrap">
               <CSSTransitionGroup
                transitionName="bank-button"
                transitionAppear={true}
                transitionAppearTimeout={600}
                transitionEnterTimeout={600}
                transitionLeave={false}>
                { returnButton() }
              </CSSTransitionGroup>
            </div>

          </div>

        </div>
      </Layout>
    );
  }
}

export default ChooseBankPage;
