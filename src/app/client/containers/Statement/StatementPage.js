import React from 'react';
import { Layout, Button, Transactions } from '../../components';
import * as Paths from '../../constants/paths';
import compareAsc from 'date-fns/compare_asc';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

class StatementPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      transactions: []
    }

    this.mergeTransactions = this.mergeTransactions.bind(this)
  }

  componentDidMount() {
    this.get(
      Paths.TRANSACTIONS,
      'GET',
      this.mergeTransactions
    )
  }

  /*
    As this is just a prototype, MVP or whatever, I don't think
    it's worth using a full HTTP library. I'll stick to the
    native XMLHttpRequest but I would use something more robust
    for production like axios, fetch or something else.
  */
  get(url, type, callback, error) {
    let xhr = new XMLHttpRequest();
    xhr.open(type, url, true);
    xhr.send();


    xhr.onload = function(e) {
      if (xhr.readyState === 4) {
        const response = JSON.parse(xhr.responseText);

        if (xhr.status === 200) {
          typeof callback == "function" && callback(response);
        } else {
          typeof callback == "function" && error(response)
        }
      }
    }
  }

  mergeTransactions (response) {
    let transactions = response.transactions.slice()

    let tempObj = {}

    response.transactions.forEach(item => tempObj[item.dateStr] = [])
    response.transactions.map(item => tempObj[item.dateStr].push(item))

    let final = Object.keys(tempObj).map(item => {
      return tempObj[item]
    })

    final.sort((a, b) => {
      return compareAsc(b[0].dateStr - a[0].dateStr)
    })

    this.setState({
      transactions: final
    })
  }

  render() {
    const transactions = this.state.transactions.map(item => {
      let day = item[0].dateStr == 'Now' ? 'Today' : item[0].dateStr
      return (
        <Transactions key={ item[0].dateStr } day={ day } transactions={ item }></Transactions>
      )
    })

    /*
      Sorry, but I couldn't get into Redux. Not enough time for the deadline as
      I never worked with it :|
      But soon enough I'll get the grasp of it!
    */
    const person = {
      firstName: 'Natwest',
      surname: 'Doe J.',
      sortCode: '12345698',
      accountNumber: '600268'
    }

    return (
      <Layout title="Nopa - Statement">
        <div className="main-content">
          <div className="hero-statement">
            <CSSTransitionGroup
              transitionName="hero-login"
              transitionAppear={true}
              transitionAppearTimeout={400}
              transitionEnterTimeout={400}
              transitionLeave={false}>
              <h1 className="hide-for-small">Which bank does this account belong to?</h1>
              <p className="hide-for-small">
                Track all of your payments by connecting as many bank accounts as you'd<br />
                like to your Nopa account and get updates on your balance instantly. Plus it's free.</p>
            </CSSTransitionGroup>
          </div>

          <CSSTransitionGroup
            transitionName="intro-statement"
            transitionAppear={true}
            transitionAppearTimeout={400}
            transitionEnterTimeout={400}
            transitionLeave={false}>
            <div className="intro">
              <div className="names">
                <div>{ person.firstName }</div>
                <div>{ person.surname }</div>
              </div>

              <div className="account">
                <div>Current account</div>
                <div>{ person.sortCode }</div>
                <div>{ person.accountNumber }</div>
              </div>
            </div>
          </CSSTransitionGroup>

          <CSSTransitionGroup
            transitionName="transactions"
            transitionAppear={true}
            transitionAppearTimeout={400}
            transitionEnterTimeout={400}
            transitionLeave={false}>
            <div className="info">
              Your transactions for the last 30 days
            </div>

            <div className="transactions">
              { transactions }
            </div>

            <Button>Show more</Button>
          </CSSTransitionGroup>
        </div>

      </Layout>
    );
  }
}

export default StatementPage;
