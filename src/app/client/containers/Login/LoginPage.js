import React from 'react';
import { Layout, LoginForm } from '../../components';
import * as Paths from '../../constants/paths';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

class LoginPage extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      errors: {}
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleSubmit (ev) {
    ev.preventDefault()
    let errors = Object.assign({}, this.state.errors)
    let properties = []
    let isValid

    errors.form = ''

    for(let error in errors) {
      if(errors[error]) isValid = false
      properties.push(error)
    }

    /*
       Nasty harcoded inputs length? Could do better but it's
       a login form and it probably won't change, so I think it's
       ok for now, but would probably seek a better way in a production
       enviroment.
    */
    isValid = (isValid === undefined && properties.length > 5) ?
      true : false

    if(isValid) {
      this.setState({ errors })
      this.context.router.push(Paths.STATEMENT)
    } else {
      errors.form = 'Please fill all the informations.'
      this.setState({ errors })
    }
  }

  handleChange (ev) {
    /*
      Honestly, I don't have much idea of how the inputs
      should be validaded as I don't know how the Britsh
      banks works.I decided to add a simple length check
      for all the inputs.
      Obviously this could be much improved.
    */
    let input = ev.target.value
    let isValid = input.length > 2 ? true : false
    let error = ''

    if(!isValid) error = `Please insert your ${ev.target.placeholder}`

    let errors = Object.assign({}, this.state.errors)
    errors[ev.target.name] = error

    this.setState({ errors })
  }

  render () {
    return (
      <Layout title="Nopa - Login">
        <div className="main-content">
          <div className="hero-login">
            <CSSTransitionGroup
              transitionName="hero-login"
              transitionAppear={true}
              transitionAppearTimeout={400}
              transitionEnterTimeout={400}
              transitionLeave={false}>
              <h1>Which bank does this account belong to?</h1>
              <p className="hide-for-small">
                Track all of your payments by connecting as many bank accounts as you'd<br />
                like to your Nopa account and get updates on your balance instantly. Plus it's free.</p>
              <p className="show-for-small">
                Enter the same details you use to login to your online banking.</p>
            </CSSTransitionGroup>
          </div>

          { this.state.errors.form &&
            <CSSTransitionGroup
              transitionName="bank-error"
              transitionAppear={true}
              transitionAppearTimeout={600}
              transitionEnterTimeout={600}
              transitionLeave={false}>
              <div className="errors">
                <span>{ this.state.errors.form }</span>
              </div>
            </CSSTransitionGroup>}

          <CSSTransitionGroup
            transitionName="login-form"
            transitionAppear={true}
            transitionAppearTimeout={400}
            transitionEnterTimeout={400}
            transitionLeave={false}>
            <LoginForm
              handleChange={ this.handleChange }
              handleSubmit={ (e) => this.handleSubmit(e) }
              errors={ this.state.errors } />
          </CSSTransitionGroup>
        </div>
      </Layout>
    );
  }
}

LoginPage.contextTypes = {
  router: React.PropTypes.object.isRequired
};

export default LoginPage;
